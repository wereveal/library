# Color Schemes
## Colorful and Balanced
   - #e27d60 <span style="background-color: #e27d60">&nbsp;&nbsp;&nbsp;&nbsp;</span>
   - #85dcbb <span style="background-color: #85dcbb">&nbsp;&nbsp;&nbsp;&nbsp;</span>
   - #e8a87c <span style="background-color: #e8a87c">&nbsp;&nbsp;&nbsp;&nbsp;</span>
   - #c38d9e <span style="background-color: #c38d9e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
   - #41b3a3 <span style="background-color: #41b3a3">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Blue w Accent Colors
- #242582 <span style="background-color: #242582">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #553d67 <span style="background-color: #553d67">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f64c72 <span style="background-color: #f64c72">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #99738e <span style="background-color: #99738e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #2f2fa2 <span style="background-color: #2f2fa2">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Natural and Earthy
- #8d8741 <span style="background-color: #8d8741">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #659dbd <span style="background-color: #659dbd">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #daad86 <span style="background-color: #daad86">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #bc986a <span style="background-color: #bc986a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fbeec1 <span style="background-color: #fbeec1">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Cool and Fresh
- #05386b <span style="background-color: #05386b">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #379683 <span style="background-color: #379683">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #5cdb95 <span style="background-color: #5cdb95">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8ee4af <span style="background-color: #8ee4af">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #edf5e1 <span style="background-color: #edf5e1">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Stylish and Sophisticated
- #5d5c61 <span style="background-color: #5d5c61">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #379683 <span style="background-color: #379683">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #7395ae <span style="background-color: #7395ae">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #557a95 <span style="background-color: #557a95">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #b1a296 <span style="background-color: #b1a296">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Elegant Yet Approchable
- #edc7b7 <span style="background-color: #edc7b7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #eee2dc <span style="background-color: #eee2dc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #bab2b5 <span style="background-color: #bab2b5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #123c69 <span style="background-color: #123c69">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ac3b61 <span style="background-color: #ac3b61">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Sleek and Futuristic
- #2c3531 <span style="background-color: #2c3531">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #116466 <span style="background-color: #116466">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d9b08c <span style="background-color: #d9b08c">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ffcb9a <span style="background-color: #ffcb9a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d1e8e2 <span style="background-color: #d1e8e2">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Minimal Warm
- #eae7dc <span style="background-color: #eae7dc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d8c3a5 <span style="background-color: #d8c3a5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8e8d8a <span style="background-color: #8e8d8a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #e98074 <span style="background-color: #e98074">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #e85a4f <span style="background-color: #e85a4f">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Energetic Blue
- #5680e9 <span style="background-color: #5680e9">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #84ceeb <span style="background-color: #84ceeb">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #5ab9ea <span style="background-color: #5ab9ea">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #c1c8e4 <span style="background-color: #c1c8e4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8860d0 <span style="background-color: #8860d0">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Corporate Blues & Browns
- #88bdbc <span style="background-color: #88bdbc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #254858 <span style="background-color: #254858">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #112d32 <span style="background-color: #112d32">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #4f4a41 <span style="background-color: #4f4a41">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #6e6658 <span style="background-color: #6e6658">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Dark Blues
- #25274d <span style="background-color: #25274d">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #464866 <span style="background-color: #464866">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #aaabb8 <span style="background-color: #aaabb8">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #2e9cca <span style="background-color: #2e9cca">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #29648a <span style="background-color: #29648a">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Teals
- #17252a <span style="background-color: #17252a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #2b7a78 <span style="background-color: #2b7a78">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #3aafa9 <span style="background-color: #3aafa9">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #def2f1 <span style="background-color: #def2f1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #feffff <span style="background-color: #feffff">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Grays n Greens
- #61892f <span style="background-color: #61892f">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #86c232 <span style="background-color: #86c232">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #222629 <span style="background-color: #222629">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #474b4f <span style="background-color: #474b4f">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #6b6e70 <span style="background-color: #6b6e70">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## More Greens
- #182628 <span style="background-color: #182628">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #65ccb8 <span style="background-color: #65ccb8">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #57ba98 <span style="background-color: #57ba98">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #3b945e <span style="background-color: #3b945e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f2f2f2 <span style="background-color: #f2f2f2">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Nature Blues
- #687864 <span style="background-color: #687864">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #31708e <span style="background-color: #31708e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #5085a5 <span style="background-color: #5085a5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8fc1e3 <span style="background-color: #8fc1e3">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f7f9fb <span style="background-color: #f7f9fb">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Pastels
- #a1c3d1 <span style="background-color: #a1c3d1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #b39bc8 <span style="background-color: #b39bc8">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f0ebf4 <span style="background-color: #f0ebf4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f172a1 <span style="background-color: #f172a1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #e64398 <span style="background-color: #e64398">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Dark Redish n Blue
- #59253a <span style="background-color: #59253a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #78244c <span style="background-color: #78244c">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #895061 <span style="background-color: #895061">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #0677a1 <span style="background-color: #0677a1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #2d4159 <span style="background-color: #2d4159">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Citrus
- #1f2605 <span style="background-color: #1f2605">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #1f6521 <span style="background-color: #1f6521">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #53900f <span style="background-color: #53900f">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #a4a71e <span style="background-color: #a4a71e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d6ce15 <span style="background-color: #d6ce15">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Lighter Blues
- #10e7dc <span style="background-color: #10e7dc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #0074e1 <span style="background-color: #0074e1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #1b9ce5 <span style="background-color: #1b9ce5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #6cdaee <span style="background-color: #6cdaee">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f79e02 <span style="background-color: #f79e02">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Petra
- #4285f4 <span style="background-color: #4285f4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #5c2018 <span style="background-color: #5c2018">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #bc4639 <span style="background-color: #bc4639">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d4a59a <span style="background-color: #d4a59a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f3e0dc <span style="background-color: #f3e0dc">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Blues n Tan
- #fbe8a6 <span style="background-color: #fbe8a6">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f4976c <span style="background-color: #f4976c">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #303c6c <span style="background-color: #303c6c">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #b4dfe5 <span style="background-color: #b4dfe5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d2fdff <span style="background-color: #d2fdff">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Warm Snow
- #b23850 <span style="background-color: #b23850">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #3b8beb <span style="background-color: #3b8beb">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #e7e3d4 <span style="background-color: #e7e3d4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #c4dbf6 <span style="background-color: #c4dbf6">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8590aa <span style="background-color: #8590aa">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Colorful
- #f78888 <span style="background-color: #f78888">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f3d250 <span style="background-color: #f3d250">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ececed <span style="background-color: #ececed">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #90ccf4 <span style="background-color: #90ccf4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #5da2d5 <span style="background-color: #5da2d5">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Muted
- #19181a <span style="background-color: #19181a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #479761 <span style="background-color: #479761">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #cebc81 <span style="background-color: #cebc81">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #a16e83 <span style="background-color: #a16e83">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #b19f9e <span style="background-color: #b19f9e">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Redish Brown
- #314455 <span style="background-color: #314455">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #644e5b <span style="background-color: #644e5b">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #9e5a63 <span style="background-color: #9e5a63">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #c96567 <span style="background-color: #c96567">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #97aabd <span style="background-color: #97aabd">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Nice Blue
- #00887a <span style="background-color: #00887a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ffccbc <span style="background-color: #ffccbc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d3e3fc <span style="background-color: #d3e3fc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #77a6f7 <span style="background-color: #77a6f7">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Brownish
- #844d36 <span style="background-color: #844d36">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #474853 <span style="background-color: #474853">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #86b3d1 <span style="background-color: #86b3d1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #aaa0a0 <span style="background-color: #aaa0a0">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8e8268 <span style="background-color: #8e8268">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Classy
- #5f6366 <span style="background-color: #5f6366">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #4d6d9a <span style="background-color: #4d6d9a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #86b3d1 <span style="background-color: #86b3d1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #99ced3 <span style="background-color: #99ced3">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #edb5bf <span style="background-color: #edb5bf">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Purples
- #802bb1 <span style="background-color: #802bb1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #2d283e <span style="background-color: #2d283e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #564f6f <span style="background-color: #564f6f">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #4c495d <span style="background-color: #4c495d">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d1d7e0 <span style="background-color: #d1d7e0">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Yellow n Teal
- #026670 <span style="background-color: #026670">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #9fedd7 <span style="background-color: #9fedd7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fef9c7 <span style="background-color: #fef9c7">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fce181 <span style="background-color: #fce181">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #edeae5 <span style="background-color: #edeae5">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Pastel Brief
- #fdf5df <span style="background-color: #fdf5df">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #5ebec4 <span style="background-color: #5ebec4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f92c85 <span style="background-color: #f92c85">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Gray Limes
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #6e6e6e <span style="background-color: #6e6e6e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #bcfd4c <span style="background-color: #bcfd4c">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Bluer
- #9cf6fb <span style="background-color: #9cf6fb">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #e1fcfd <span style="background-color: #e1fcfd">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #394f8a <span style="background-color: #394f8a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #4a5fc1 <span style="background-color: #4a5fc1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #e5b9a8 <span style="background-color: #e5b9a8">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ead6cd <span style="background-color: #ead6cd">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Mono Blue
- #151f6d <span style="background-color: #151f6d">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #00abe1 <span style="background-color: #00abe1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d2fdff <span style="background-color: #d2fdff">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Lots o Blue
- #150734 <span style="background-color: #150734">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #0f2557 <span style="background-color: #0f2557">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #28559a <span style="background-color: #28559a">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #3778c2 <span style="background-color: #3778c2">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #4b9fe1 <span style="background-color: #4b9fe1">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #63bce5 <span style="background-color: #63bce5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #7ed5ea <span style="background-color: #7ed5ea">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Pinkish
- #000 <span style="background-color: #000">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fa255e <span style="background-color: #fa255e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #c39ea0 <span style="background-color: #c39ea0">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f8e5e5 <span style="background-color: #f8e5e5">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Three Greens
- #5daa68 <span style="background-color: #5daa68">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #3f6844 <span style="background-color: #3f6844">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #faf1cf <span style="background-color: #faf1cf">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Pink n Blue
- #ee7879 <span style="background-color: #ee7879">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #2a3166 <span style="background-color: #2a3166">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f4abaa <span style="background-color: #f4abaa">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #cae7df <span style="background-color: #cae7df">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Three Blues
- #182978 <span style="background-color: #182978">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #6688cc <span style="background-color: #6688cc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #acbfe6 <span style="background-color: #acbfe6">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Mutted Greens
- #f5f5f5 <span style="background-color: #f5f5f5">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #8db48e <span style="background-color: #8db48e">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #4d724d <span style="background-color: #4d724d">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Blue Four
- #212221 <span style="background-color: #212221">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #1181b2 <span style="background-color: #1181b2">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ddedf4 <span style="background-color: #ddedf4">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #44449b <span style="background-color: #44449b">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Sky
- #4dbdeb <span style="background-color: #4dbdeb">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #dfeeee <span style="background-color: #dfeeee">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ffe08c <span style="background-color: #ffe08c">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Greenish
- #03363d <span style="background-color: #03363d">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #174d4d <span style="background-color: #174d4d">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f8f9f9 <span style="background-color: #f8f9f9">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Black'nWhite
- #010101 <span style="background-color: #010101">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #676767 <span style="background-color: #676767">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #f9f9f9 <span style="background-color: #f9f9f9">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #ede8e4 <span style="background-color: #ede8e4">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Good Contrasts
- #1e6ba2 <span style="background-color: #1e6ba2">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #be8b09 <span style="background-color: #be8b09">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #68757b <span style="background-color: #68757b">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Violet
- #4f2361 <span style="background-color: #4f2361">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #7f5281 <span style="background-color: #7f5281">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #28292e <span style="background-color: #28292e">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Silver
- #c6c6c6 <span style="background-color: #c6c6c6">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #706f6f <span style="background-color: #706f6f">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #fff <span style="background-color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #d2daed <span style="background-color: #d2daed">&nbsp;&nbsp;&nbsp;&nbsp;</span>

## Dirt
- #ded9d6 <span style="background-color: #ded9d6">&nbsp;&nbsp;&nbsp;&nbsp;</span>
- #252d35 <span style="background-color: #252d35">&nbsp;&nbsp;&nbsp;&nbsp;</span>