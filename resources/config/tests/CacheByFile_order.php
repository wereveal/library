<?php
return [
    'createFilePath',
    'getCachePath',
    'getCacheType',
    'setDefaultTtl',
    'getDefaultTtl',
    'set',
    'get',
    'setMultiple',
    'getMultiple',
    'getMultipleByPrefix',
    'has',
    'clearByKeyPrefix',
    'cleanExpiredFiles',
    'clear'
];