# Flow for Blog
## Creating a blog post
- Open the blog post web page and fill in the info (use code for page input?)
    - Title
    - Content
    - Author?
    - Date?
    - Publish Date
    - Unpublish Date
    - Category - pull down, grouped under the navgroup 'blog'
    - Sub-category - pull down, children of categories
    - Note that both category and sub-categories have to be added in the manager.
- Save 
    - Create a unique url based on Category/sub-category/title
    - Content saved
## Displaying Home page
- search for featured blog post
- search for most recent post for each category
- fill the content of featured blog post
## Database use
- lib_urls (/category/sub-category/../blog-title/)
- lib_navgroups (primary navgroup is blog which normally doesn't appear in the navigation)
- lib_navigation (categories and sub-categories)
- lib_nav_ng_map ()
- lib_page
- lib_content
- lib_routes
    - route_class = BlogController
        - route_method = route
        - route_action = home, article, search_by_category, search_by_title, search_by_tag
    - route_class = HomeController
        - route_method = route
        - route_action = blog
- lib_tags (new - for a search) 
- lib_tags_content_map (maps tags to content)