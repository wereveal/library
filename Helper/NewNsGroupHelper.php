<?php /** @noinspection PhpUnused */

/**
 * Class NewNsgroup_helper
 * @package Ritc_Library
 */
namespace Ritc\Library\Helper;

use Ritc\Library\Exceptions\CustomException;
use Ritc\Library\Exceptions\HelperException;
use Ritc\Library\Exceptions\ModelException;
use Ritc\Library\Models\GroupsModel;
use Ritc\Library\Models\PageModel;
use Ritc\Library\Models\PeopleComplexModel;
use Ritc\Library\Models\TwigComplexModel;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Services\Di;

/**
 * Helper for setting up a new namespace group (nsgroup).
 *
 * @author  William E Reveal <bill@revealitconsulting.com>
 * @version 3.0.0
 * @date    2023-05-02 09:40:24 
 * ## Change Log
 * - v3.0.0         - Renamed to NewNsGroupHelper                                   - 2023-05-02 wer
 * - v2.0.0         - updated for php8                                              - 2021-11-29 wer
 * - v1.1.2         - Bug Fixes                                                     - 2019-08-19 wer
 * - v1.1.1         - Bug fixes                                                     - 2018-05-29 wer
 * - v1.1.0         - Create Default Files changed to use over all site variable    - 2018-04-14 wer
 * - v1.0.0         - Initial Production version                                    - 2017-12-15 wer
 * - v1.0.0-alpha.0 - Initial version                                               - 2017-11-24 wer
 */
class NewNsGroupHelper
{
    /** @var array $a_config */
    private array $a_config = [];
    /** @var  array $a_new_dirs */
    private array $a_new_dirs;
    /** @var  string $nsgroup_path */
    private string $nsgroup_path;
    /** @var  string $htaccess_text */
    private string $htaccess_text;
    /** @var  string $keep_me_text */
    private string $keep_me_text;
    /** @var Di $o_di */
    private Di $o_di;
    /** @var  string $tpl_text */
    private string $tpl_text;

    /**
     * NewNsgroup_helper constructor.
     *
     * @param Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->o_di = $o_di;
    }

    /**
     * Updates the home page record with the namespace group (nsgroup) index template.
     *
     * @throws ModelException
     */
    public function changeHomePageTpl():void
    {
        /** @var DbModel $o_db */
        $o_db = $this->o_di->get('db');
        $o_page = new PageModel($o_db);
        try {
            $o_tc = new TwigComplexModel($this->o_di);
            $a_tpl = $o_tc->readTplInfoByName('index', $this->a_config['nsgroup_twig_prefix']);
            $tpl_id = $a_tpl['tpl_id'];
        }
        catch (ModelException $e) {
            $error_msg = ' In changeHomePageTpl::readTplInfoByName: ' . $e->getMessage();
            $error_msg .= "\n" . var_export($this->a_config, true);
            throw new ModelException($error_msg, $e->getCode(), $e);
        }
        $table = $o_page->getDbTable();
        $sql = "
            UPDATE {$table}
            SET tpl_id = :tpl_id
            WHERE page_title LIKE :page_title
        ";
        $a_values = ['tpl_id' => $tpl_id, 'page_title' => '%Home%'];
        try {
            $o_db->update($sql, $a_values);
        }
        catch (ModelException $e) {
            $error_message = $e->getMessage();
            $error_message .= "\nsql: \n" . $sql;
            $error_message .= "\nvalues: \n" . var_export($a_values, true);
            $error_code = ExceptionHelper::getCodeNumberModel('update_unspecified');
            throw new ModelException($error_message, $error_code, $e);
        }
    }

    /**
     * Creates directories for the new namespace group (nsgroup).
     *
     * @param array $a_new_dirs optional, defaults to class property $a_new_dirs
     * @return bool
     */
    public function createDirectories(array $a_new_dirs = []):bool
    {
        if (empty($a_new_dirs)) {
            $a_new_dirs = $this->a_new_dirs;
        }
        if (!file_exists($this->nsgroup_path) &&
            !mkdir($this->nsgroup_path, 0755, true) &&
            !is_dir($this->nsgroup_path)
        ) {
            return false;
        }
        foreach ($a_new_dirs as $dir) {
            $new_dir = $this->nsgroup_path . '/' . $dir;
            if (!file_exists($new_dir) &&
                !mkdir($new_dir, 0755, true) &&
                !is_dir($new_dir)
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Creates the default files for the namespace group (nsgroup).
     *
     * @param bool $is_site optional, defaults to false
     * @return bool
     * @noinspection PhpComplexFunctionInspection
     */
    public function createDefaultFiles(bool $is_site):bool
    {
        if (empty($this->a_new_dirs)
            || empty($this->nsgroup_path)
            || empty($this->htaccess_text)
            || empty($this->tpl_text)
            || empty($this->a_config)
            || empty($this->a_config['namespace'])
            || empty($this->a_config['nsgroup_name'])
        ) {
            return false;
        }
        if (file_exists($this->nsgroup_path)) {
            if (file_put_contents($this->nsgroup_path . '/.htaccess', $this->htaccess_text)) {
                foreach ($this->a_new_dirs as $dir) {
                    $dir = $this->nsgroup_path . '/' . $dir;
                    $new_file = $dir . '/.keepme';
                    $new_tpl  = $dir . '/no_file.twig';
                    if (file_exists($dir)) {
                        if (str_contains($dir, 'templates')) {
                            if (!file_put_contents($new_tpl, $this->tpl_text)) {
                                return false;
                            }
                        }
                        elseif (!file_put_contents($new_file, $this->keep_me_text)) {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
            }
            $a_find = [
                '{NAMESPACE}',
                '{NSGROUP}',
                '{namespace}',
                '{nsgroup_name}',
                '{controller_name}',
                '{controller_method}',
                '{controller_use}',
                '{controller_vars}',
                '{controller_construct}',
                '{author}',
                '{sauthor}',
                '{email}',
                '{idate}',
                '{sdate}',
                '{twig_prefix}'
            ];
            $a_replace = [
                $this->a_config['namespace'],
                $this->a_config['nsgroup_name'],
                strtolower($this->a_config['namespace']),
                strtolower($this->a_config['nsgroup_name']),
                '',
                '',
                '',
                '',
                '',
                $this->a_config['author'],
                $this->a_config['short_author'],
                $this->a_config['email'],
                date('Y-m-d H:i:s'),
                date('Y-m-d'),
                $this->a_config['nsgroup_twig_prefix']

            ];

            ### Create the main controller for the namespace group (nsgroup) ###
            $controller_file = ucfirst(strtolower($this->a_config['nsgroup_name'])) . 'Controller.php';
            if ($is_site) {
                $controller_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/MainController.php.txt');
            }
            else {
                $a_replace[4] = $this->a_config['nsgroup_name'];
                $a_replace[5] = file_get_contents(SRC_CONFIG_PATH . '/install_files/main_controller.snippet');
                $controller_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/controller.php.txt');
            }
            $controller_text = str_replace($a_find, $a_replace, $controller_text);
            if (!file_put_contents($this->nsgroup_path . '/Controllers/' . $controller_file, $controller_text)) {
                return false;
            }
            ### Create the home controller for the namespace group (nsgroup) ###
            $controller_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/HomeController.php.txt');
            if ($controller_text) {
                $controller_text = str_replace($a_find, $a_replace, $controller_text);
                if (!file_put_contents($this->nsgroup_path . '/Controllers/HomeController.php', $controller_text)) {
                    return false;
                }
            }
            else {
                return false;
            }

            ### Create the manager controller for the namespace group (nsgroup) ###
            $controller_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/ManagerController.php.txt');
            if ($controller_text) {
                $controller_text = str_replace($a_find, $a_replace, $controller_text);
                if (!file_put_contents($this->nsgroup_path . '/Controllers/ManagerController.php', $controller_text)) {
                    return false;
                }
            }
            else {
                return false;
            }

            ### Create the home view for the namespace group (nsgroup) ###
            $view_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/HomeView.php.txt');
            if ($view_text) {
                $view_text = str_replace($a_find, $a_replace, $view_text);
                if (!file_put_contents($this->nsgroup_path . '/Views/HomeView.php', $view_text)) {
                    return false;
                }
            }
            else {
                return false;
            }

            ### Create the manager view for the namespace group (nsgroup) ###
            $view_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/ManagerView.php.txt');
            if ($view_text) {
                $view_text = str_replace($a_find, $a_replace, $view_text);
                if (!file_put_contents($this->nsgroup_path . '/Views/ManagerView.php', $view_text)) {
                    return false;
                }
            }
            else {
                return false; }

            ### Create the default scss files for namespace group (nsgroup) ###
            $ns_name             = strtolower($this->a_config['namespace']);
            $nsgroup_name        = strtolower($this->a_config['nsgroup_name']);
            $scss_path           = $this->nsgroup_path . '/resources/assets/scss/';
            $styles_text         = file_get_contents(SRC_CONFIG_PATH . '/install_files/styles.scss.txt');
            $styles_manager_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/styles_manager.scss.txt');
            $colors_forward_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/_colors.scss.txt');
            $mq_forward_text     = file_get_contents(SRC_CONFIG_PATH . '/install_files/_media_queries.scss.txt');
            $mixins_forward_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/_mixins.scss.txt');
            $vars_forward_text   = file_get_contents(SRC_CONFIG_PATH . '/install_files/_variables.scss.txt');
            $common_text         = file_get_contents(SRC_CONFIG_PATH . '/install_files/nsgroup_common.scss.txt');
            $vars_text           = file_get_contents(SRC_CONFIG_PATH . '/install_files/nsgroup_variables.scss.txt');
            $colors_text         = file_get_contents(SRC_CONFIG_PATH . '/install_files/nsgroup_colors.scss.txt');
            $nav_text            = file_get_contents(SRC_CONFIG_PATH . '/install_files/nsgroup_nav.scss.txt');
            $styles_text         = str_replace('{nsgroup_name}', $nsgroup_name, $styles_text);
            $styles_manager_text = str_replace('{nsgroup_name}', $nsgroup_name, $styles_manager_text);
            $vars_text           = str_replace('{nsgroup_name}', $nsgroup_name, $vars_text);
            $nav_text            = str_replace('{nsgroup_name}', $nsgroup_name, $nav_text);
            $colors_text         = str_replace('{nsgroup_name}', $nsgroup_name, $colors_text);
            $colors_forward_text = str_replace('{nsgroup_name}', $nsgroup_name, $colors_forward_text);
            $mixins_forward_text = str_replace('{nsgroup_name}', $nsgroup_name, $mixins_forward_text);
            $vars_forward_text   = str_replace('{nsgroup_name}', $nsgroup_name, $vars_forward_text);
            $mq_forward_text     = str_replace('{nsgroup_name}', $nsgroup_name, $mq_forward_text);
            $file_name = $ns_name . '_' . $nsgroup_name . '.scss';
            file_put_contents($scss_path . $file_name, $styles_text);
            $file_name = $ns_name . '_' . $nsgroup_name . '_manager.scss';
            file_put_contents($scss_path . $file_name, $styles_manager_text);
            file_put_contents($scss_path . '_colors.scss', $colors_forward_text);
            file_put_contents($scss_path . '_mixins.scss', $mixins_forward_text);
            file_put_contents($scss_path . '_nav.scss', $nav_text);
            file_put_contents($scss_path . '_variables.scss', $vars_forward_text);
            file_put_contents($scss_path . '_media_queries.scss', $mq_forward_text);
            $more_text = '// namespace group (nsgroup) specific';
            file_put_contents($scss_path . 'nsgroup/_' . $nsgroup_name . '.scss', $more_text);
            file_put_contents($scss_path . 'nsgroup/_colors.scss', $colors_text);
            file_put_contents($scss_path . 'nsgroup/_forms.scss', $more_text . ' forms');
            file_put_contents($scss_path . 'nsgroup/_functions.scss', $more_text . ' functions');
            file_put_contents($scss_path . 'nsgroup/_media_queries.scss', $more_text . ' media queries');
            file_put_contents($scss_path . 'nsgroup/_mixins.scss', $more_text . ' mixins');
            file_put_contents($scss_path . 'nsgroup/_common.scss', $common_text);
            file_put_contents($scss_path . 'nsgroup/_variables.scss', $vars_text);

            ### Create the twig_config file ###
            $twig_file = file_get_contents(SRC_CONFIG_PATH . '/install_files/twig_config.php.txt');
            if ($twig_file) {
                $new_twig_file = str_replace($a_find, $a_replace, $twig_file);
                if (!file_put_contents($this->nsgroup_path . '/resources/config/twig_config.php', $new_twig_file)) {
                    return false;
                }
            }
            else {
                return false;
            }

            ### Copy main twig files ###
            $nsgroup_theme = $this->a_config['nsgroup_theme_name'] ?? 'base';
            $nsgroup_theme_file = '/templates/themes/' . $nsgroup_theme . '.twig';
            if (strpos($nsgroup_theme, 'fixed')) {
                $base_twig = '/templates/themes/base_fixed.twig';
            }
            elseif (strpos($nsgroup_theme, 'bul')) {
                $base_twig = '/templates/themes/base_bul.twig';
            }
            else {
                $base_twig = '/templates/themes/base.twig';
            }
            $resource_path = $this->nsgroup_path . '/resources';
            $twig_text = file_get_contents(SRC_PATH . $base_twig);
            $short_name = $ns_name . '_' . $nsgroup_name;
            $new_base_tpl = $short_name . '.css';
            $twig_text = str_replace('styles.css', $new_base_tpl, $twig_text);
            if ($twig_text && !file_put_contents($resource_path . $nsgroup_theme_file, $twig_text)) {
                return false;
            }
            $default_templates_path = SRC_PATH . '/templates/pages/';
            $a_default_files = scandir($default_templates_path, SCANDIR_SORT_ASCENDING);
            $pages_path = $resource_path . '/templates/pages/';
            foreach ($a_default_files as $this_file) {
                if ($this_file !== '.' && $this_file !== '..') {
                    $twig_text = file_get_contents($default_templates_path . $this_file);
                    $twig_text = str_replace('styles_', $short_name . '_', $twig_text);
                    if ($twig_text && !file_put_contents($pages_path . $this_file, $twig_text)) {
                        return false;
                    }
                }
            }

            if ($is_site) {
                $a_find = [
                    '{NAMESPACE}',
                    '{NSGROUP}'
                ];
                $a_replace = [
                    $this->a_config['namespace'],
                    $this->a_config['nsgroup_name']
                ];
                $index_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/index.php.txt');
                if ($index_text) {
                    $index_text = str_replace($a_find, $a_replace, $index_text);
                    if (!file_put_contents(PUBLIC_PATH . '/index.php', $index_text)) {
                        return false;
                    }
                }
                $db_config_file = empty($this->a_config['db_file'])
                    ? 'db_config'
                    : $this->a_config['db_file'];
                $public_path = empty($this->a_config['public_path'])
                    ? '$_SERVER["DOCUMENT_ROOT"]'
                    : $this->a_config['public_path'];
                $base_path = empty($this->a_config['base_path'])
                    ? 'dirname(PUBLIC_PATH)'
                    : $this->a_config['base_path'];
                $developer_mode = isset($this->a_config['developer_mode']) && $this->a_config['developer_mode'] === 'true'
                    ? 'true'
                    : 'false';
                $server_http_host = empty($this->a_config['server_http_host'])
                    ? ''
                    : $this->a_config['server_http_host'];
                $domain = empty($this->a_config['domain'])
                    ? ''
                    : $this->a_config['domain'];
                $tld = empty($this->a_config['tld'])
                    ? 'com'
                    : $this->a_config['tld'];
                $specific_host = empty($this->a_config['specific_host']) // used mostly with MAMP and single name urls e.g. https://testsite/
                    ? ''
                    : $this->a_config['specific_host'];
                $a_find = [
                    '{db_config_file}',
                    '{public_path}',
                    '{base_path}',
                    '{developer_mode}',
                    '{server_http_host}',
                    '{domain}',
                    '{tld}',
                    '{specific_host}',
                    '{host_text}'
                ];
                $a_replace = [
                    $db_config_file,
                    $public_path,
                    $base_path,
                    $developer_mode,
                    $server_http_host,
                    $domain,
                    $tld,
                    $specific_host,
                    ''
                ];
                if (!empty($specific_host)) {
                    $a_replace[8] = "\n    case '$specific_host':";
                }
                $setup_text = file_get_contents(SRC_CONFIG_PATH . '/install_files/setup.php.txt');
                $setup_text = str_replace($a_find, $a_replace, $setup_text);
                file_put_contents(PUBLIC_PATH . '/setup.php', $setup_text);
            }
            return true;
        }
        return false;
    }

    /**
     * @return array
     * @throws HelperException
     */
    public function createTwigDbRecords(): array
    {
        $o_tcm = new TwigComplexModel($this->o_di);
        $nsgroup_resource_dir = str_replace(BASE_PATH, '', $this->nsgroup_path) . '/resources/templates';
        $a_values = [
           'tp_prefix'  => $this->a_config['nsgroup_twig_prefix'],
           'tp_path'    => $nsgroup_resource_dir,
           'tp_active'  => 'true',
           'tp_default' => $this->a_config['main_twig'],
           'theme_name' => $this->a_config['nsgroup_theme_name']
        ];
        # print_r($a_values);
        try {
            return $o_tcm->createTwigForNsgroup($a_values);
        }
        catch (ModelException $e) {
            $error_message = $e->getMessage();
            $error_code = ExceptionHelper::getCodeNumberModel('update_unspecified');
            throw new HelperException($error_message, $error_code, $e);
        }
    }

    /**
     * Creates Users and possibly groups from the data.
     *
     * @return string
     * @throws HelperException
     */
    public function createUsers(): string
    {
        try {
            $o_people = new PeopleComplexModel($this->o_di);
        }
        catch (CustomException $e) {
            throw new HelperException($e->getMessage(), $e->getCode(), $e);
        }
        /** @var DbModel $o_db */
        $o_db = $this->o_di->get('db');
        $o_groups = new GroupsModel($o_db);

        $a_people = empty($this->a_config['a_users'])
            ? []
            : $this->a_config['a_users'];
        $a_groups = empty($this->a_config['a_groups'])
            ? []
            : $this->a_config['a_groups'];
        if (empty($a_people) && empty($a_groups)) {
            return 'No users to save';
        }
        if (!empty($a_groups)) {
            try {
                $a_found_groups = $o_groups->read();
            }
            catch (ModelException $e) {
                throw new HelperException('Could not read the groups: ' . $e->getMessage(), $e->getCode(), $e);
            }
            foreach ($a_found_groups as $a_found_group) {
                $value = $a_found_group['group_name'];
                $found_key = Arrays::inArrayRecursive($value, $a_groups, true);
                if ($found_key !== false) {
                    unset($a_groups[$found_key]);
                }
            }
            try {
                $o_groups->create($a_groups);
            }
            catch (ModelException $e) {
                throw new HelperException('Could not save the groups: ' . $e->getMessage(), $e->getCode(), $e);
            }
        }
        if (!empty($a_people)) {
            foreach ($a_people as $a_person) {
                try {
                    $a_groups = $o_groups->readByName($a_person['group_name']);
                    unset($a_person['group_name']);
                    $a_person['groups'] = [$a_groups[0]['group_id']];
                    $o_people->savePerson($a_person);
                }
                catch (ModelException $e) {
                    throw new HelperException('A problem occurred trying to save the person: ' . $e->errorMessage(),
                                              $e->getCode(),
                                              $e
                    );
                }
            }
            return 'Success!';
        }
        throw new HelperException('Unknown error occurred.', 999);
    }

    /**
     * Standard class property SETter, nsgroup_path.
     *
     * @param string $value
     */
    public function setNsgroup(string $value = ''):void
    {
        if (!empty($value)) {
            $this->nsgroup_path = $value;
        }
        elseif (!empty($this->a_config['nsgroup_path'])) {
            $this->nsgroup_path = $this->a_config['nsgroup_path'];
        }
        elseif (!empty($this->a_config['namespace'])
                && !empty($this->a_config['nsgroup_name'])) {
            $this->nsgroup_path = NAMESPACES_PATH
                     . '/'
                     . $this->a_config['namespace']
                     . '/'
                     . $this->a_config['nsgroup_name'];
        }
    }

    /**
     * Standard GETter for nsgroup_path
     *
     * @return string
     */
    public function getNsgroup():string
    {
        return $this->nsgroup_path;
    }

    /**
     * Standard class property SETter for a_values.
     *
     * @param array $a_values Optional, defaults to a preset bunch of values.
     */
    public function setConfig(array $a_values = []):void
    {
        $a_default = [
            'nsgroup_name'      => 'Main',                          // specify the primary namespace group (nsgroup) to which generates the home page
            'nsgroup_them_name' => 'base',                          // default colors for namespace group (nsgroup)
            'namespace'         => 'Ritc',                          // specify the root namespace the nsgroup will be in
            'author'            => 'William E Reveal',              // specify the author of the nsgroup
            'short_author'      => 'wer',                           // abbreviation for the author
            'email'             => '<bill@revealitconsulting.com>', // email of the author
            'public_path'       => '',                              // leave blank for default setting
            'base_path'         => '',                              // leave blank for default setting
            'sever_http_host'   => '',                              // $_SERVER['HTTP_HOST'] results or leave blank for default
            'domain'            => 'revealitconsulting',            // domain name of site
            'tld'               => 'com',                           // top level domain, e.g., com, net, org
            'specific_host'     => '',                              // e.g. www, test
            'developer_mode'    => 'false',                         // affects debugging messages
            'main_nsgroup'      => 'false',                         // specifies if this nsgroup is the one called by /index.php and specifies the WebController
            'main_twig'         => 'false'                          // specifies if this nsgroup twig prefix is the default (if false, site_ is default)
        ];

        if (empty($a_values)) {
            $a_values = $a_default;
        }
        foreach ($a_default as $key => $value) {
            if (empty($a_values[$key])) {
                $a_values[$key] = $value;
            }
        }
        if (empty($a_values['nsgroup_twig_prefix'])) {
            $a_values['nsgroup_twig_prefix'] = strtolower($a_values['nsgroup_name']) . '_';
        }
        $this->a_config = $a_values;
    }

    /**
     * Standard GETter for a_config
     *
     * @return array
     */
    public function getConfig():array
    {
        return $this->a_config;
    }

    /**
     * Standard class property SETter, htaccess_text.
     *
     * @param string $value
     */
    public function setHtaccessText(string $value = ''):void
    {
        if ($value === '') {
            $value =<<<EOF
<IfModule mod_authz_core.c>
    Require all denied
</IfModule>
<IfModule !mod_authz_core.c>
    Order deny,allow
    Deny from all
</IfModule>
EOF;
        }
        $this->htaccess_text = $value;
    }

    /**
     * Standard GETter for htaccess_text
     * 
     * @return string
     */
    public function getHtaccessText():string
    {
        return $this->htaccess_text;
    }

    /**
     * Standard class property SETter, keepme_text.
     *
     * @param string $value
     */
    public function setKeepMeText(string $value = ''):void
    {
        if ($value === '') {
            $value = 'Place Holder';
        }
        $this->keep_me_text = $value;
    }

    /**
     * Standard GETter for keep_me_text
     *
     * @return string
     */
    public function getKeepMeText():string
    {
        return $this->keep_me_text;
    }

    /**
     * Standard class property SETter, a_new_dirs.
     * @param array $a_values
     */
    public function setNewDirs(array $a_values = []):void
    {
        if (empty($a_values)) {
            $a_values = [
                'Abstracts',
                'Controllers',
                'Entities',
                'Interfaces',
                'Models',
                'Tests',
                'Traits',
                'Views',
                'resources',
                'resources/assets',
                'resources/assets/images',
                'resources/assets/js',
                'resources/assets/scss',
                'resources/assets/scss/nsgroup',
                'resources/assets/txt',
                'resources/config',
                'resources/sql',
                'resources/templates',
                'resources/templates/elements',
                'resources/templates/pages',
                'resources/templates/forms',
                'resources/templates/snippets',
                'resources/templates/tests',
                'resources/templates/themes'
            ];
        }
        $this->a_new_dirs = $a_values;
    }
    
    /**
     * Standard GETter for a_new_dirs
     *
     * @return array
     */
    public function getNewDirs():array
    {
        return $this->a_new_dirs;
    }

    /**
     * Standard class property SETter, tpl_text.
     *
     * @param string $value
     */
    public function setTplText(string $value = ''):void
    {
        if ($value === '') {
            $value = '<h3>An Error Has Occurred</h3>';
        }
        $this->tpl_text = $value;
    }

    /**
     * Standard GETter for a tpl_text
     *
     * @return string
     */
    public function getTplText():string
    {
        return $this->tpl_text;
    }

    /**
     * Sets the class properties that are needed.
     *
     * @param array $a_config
     * @throws HelperException
     */
    public function setupProperties(array $a_config):void
    {
        if (empty($a_config)) {
            throw new HelperException('Required value missing.', 100);
        }
        $this->a_config = $a_config;
        $this->setNsgroup();
        $this->setNewDirs();
        $this->setHtaccessText();
        $this->setKeepMeText();
        $this->setTplText();
    }
}
