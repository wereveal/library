<?php
namespace Ritc\Library\Traits;

/**
 * CacheTesterTraits common tests based on CacheTraits
 *
 * @author William E Reveal <bill@revealitconsulting.com>
 * @version 1.0.0-beta.1
 * @date 2022-08-25 14:56:09
 * ## Change Log
 * - v1.0.0-beta.1 - initial version                            - 2022-08-25 wer
 */
trait CacheTesterTraits
{
    public function clearTest():string
    {
        return 'failed';
    }

    public function clearByKeyPrefixTest():string
    {
        return 'failed';
    }

    public function cleanExpiredFilesTest():string
    {
        return 'failed';
    }

    public function getMultipleTest():string
    {
        return 'failed';
    }

    public function getMultipleByPrefixTest():string
    {
        return 'failed';
    }

    public function setMultipleTest():string
    {
        return 'failed';
    }

    public function deleteMultipleTest():string
    {
        return 'failed';
    }

    public function hasTest():string
    {
        return 'failed';
    }

    public function setupCacheTest():string
    {
        return 'failed';
    }

    public function getCacheTypeTest():string
    {
        return 'failed';
    }

    public function getDefaultTtlTest():string
    {
        return 'failed';
    }

    public function setDefaultTtlTest():string
    {
        return 'failed';
    }

    public function deleteFilesTest():string
    {
        return 'failed';
    }

    public function getCachePathTest():string
    {
        return 'failed';
    }

}